# go-scfg

[![Go Reference](https://pkg.go.dev/badge/codeberg.org/emersion/go-scfg.svg)](https://pkg.go.dev/codeberg.org/emersion/go-scfg)

Go library for [scfg].

## License

MIT

[scfg]: https://git.sr.ht/~emersion/scfg
